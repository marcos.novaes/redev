const TOKEN_KEY = "x-auth-token";

const getToken = () => localStorage.getItem(TOKEN_KEY);

const saveToken = (token) => localStorage.setItem(TOKEN_KEY, token);

const removeToken = () => localStorage.removeItem(TOKEN_KEY);

const isAuthenticated = () => {
  return getToken() !== null;
};

const getUser = async () => {
  const user = getToken();
  console.log(user);
  return undefined;
};

export { isAuthenticated, getToken, saveToken, removeToken, getUser };
