import { Layout, Menu, Breadcrumb } from 'antd';
import { useState } from 'react';
import styled from 'styled-components'

import { CgProfile } from 'react-icons/cg'
import { BsListUl, BsCodeSlash } from 'react-icons/bs'
import { MdRssFeed } from 'react-icons/md'
import history from '../../config/history';

const { Header, Content, Footer, Sider } = Layout;

const BaseLayout = ({ children }) => {
    const [collapsed, setCollapsed] = useState(false)

    const onCollapse = (collapsed) => {
        console.log(collapsed);
        setCollapsed(collapsed);
    };


    const rotas = {
        "feed": {
            "rota": "/",
            "order": "1"
        },
        "perfil": {
            "rota": "/perfil",
            "order": "2"
        },
        "topicos": {
            "rota": "/topicos",
            "order": "3"
        }
    }


    const currentPage = Object.entries(rotas).filter((m) => m[1].rota === history.location.pathname)[0][1].order;



    const changePage = (rota) => {
        const rotaToGo = rotas[rota].rota
        history.push(`${rotaToGo}`)
    }


    return (
        <Layout style={{ height: '100vh' }}>
            <SiderEstilizada collapsible collapsed={collapsed} onCollapse={() => onCollapse(!collapsed)}>
                <div className="logo" />
                <Menu theme="dark" defaultSelectedKeys={['1']} selectedKeys={[currentPage]} mode="inline">
                    <MenuItemEstilo key="1" onClick={() => changePage('feed')}  >
                        <MdRssFeed />{collapsed ? "" : "Feed"}
                    </MenuItemEstilo>
                    <MenuItemEstilo key="3" onClick={() => changePage('topicos')} >
                        <BsListUl />{collapsed ? "" : "Tópicos"}
                    </MenuItemEstilo>
                    <MenuItemEstilo key="2" onClick={() => changePage('perfil')}  >
                        <CgProfile />{collapsed ? "" : "Perfil"}
                    </MenuItemEstilo>
                </Menu>
            </SiderEstilizada>
            <Layout className="site-layout">
                <HeaderEstilo className="site-layout-background"  >
                    <BsCodeSlashEstilo />  ReDDev
                </HeaderEstilo>
                <ContentEstilo >
                    {children}
                </ContentEstilo>
                <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
            </Layout>
        </Layout>
    )

}

const SiderEstilizada = styled(Sider)`
    padding-top: 60px;

`
const HeaderEstilo = styled(Header)`
    padding: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 30px;
    color: rgba(255, 255, 255, 0.65);

`

const BsCodeSlashEstilo = styled(BsCodeSlash)`

    margin-right: 10px;    

`

const MenuItemEstilo = styled(Menu.Item)`

    font-size: 30px;
    padding: 30px 0px;

    display: flex;
    align-items: center;
    justify-content: space-between;
  

`


const ContentEstilo = styled(Content)`

    margin: 0px 0px 0px 16px;
    padding-right: 16px;
    height: 100vh;
    overflow: scroll;

`


export default BaseLayout