import { Card, Tag } from "antd"
import styled from 'styled-components'
import { TiThumbsOk, TiThumbsDown } from 'react-icons/ti'

const SinglePost = (props) => {
    return (
        <CardEstilo title="Default size card" extra={<Tag color="geekblue">Topico</Tag>}>
            <p>Card content Card content Card content Card content Card content Card content Card content
            Card content Card content Card content Card contentCard contentCard content
            Card content Card content Card content Card content Card content Card content Card content
            Card content Card content Card content Card contentCard contentCard content
            </p>
            <DivLikeDeslike>
                <TiThumbsOk />
                <TiThumbsDown />
            </DivLikeDeslike>

        </CardEstilo>
    )
}

const CardEstilo = styled(Card)`

    margin: 15px 0px;
    box-shadow: 7px 9px 21px -5px rgba(166,164,166,1);
`

const DivLikeDeslike = styled.div`
    font-size: 20px;
    bottom: 0px;
    position: absolute;
    
    svg{
        cursor: pointer;
        margin-right: 10px;
    }

`


export default SinglePost