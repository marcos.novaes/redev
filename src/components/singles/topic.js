import { Card } from "antd"
import styled from 'styled-components'

const SingleTopic = ({ topic, handleClick }) => {
    return (
        <CardEstilo title={topic.title} size="small" onClick={() => handleClick(topic)}>
            <h4>{topic.participants_count} seguidores</h4>
            <p>{topic.description}
            </p>


        </CardEstilo>
    )
}

const CardEstilo = styled(Card)`

    margin: 15px 0px;
    box-shadow: 7px 9px 21px -5px rgba(166,164,166,1);
    width: 350px;
    height: 250px;
    cursor: pointer;
`



export default SingleTopic