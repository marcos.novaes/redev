import { Layout, Form, Input, Button, Select } from "antd";
import styled from 'styled-components'
import { BsCodeSlash } from 'react-icons/bs'
import history from "../../config/history";
import { useState } from "react";
import { signUpService } from '../../service/user'
import { useDispatch } from "react-redux";
import { signIn } from "../../store/Sign/sign.action";


const SignUpForm = (props) => {
    const dispatch = useDispatch();


    const [form, setForm] = useState({})



    const handleChange = (e) => {
        const value = e.target ? e.target.value : e
        const name = e.target ? e.target.name : "gender"
        setForm({
            ...form,
            [name]: value
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            const result = await signUpService(form)
            const dataLogin = {
                "email": form.email,
                "password": form.password
            }
            dispatch(signIn(dataLogin));
            console.log(result);
        } catch (error) {
            console.log(error)
        }

    }



    return (
        <LayoutEstilo className="layout">
            <FormCadastro>
                <h2><BsCodeSlash />  ReDDev  |  Nova Conta</h2>
                <FormEstilo>
                    <Form.Item
                        name="name"
                        label="Name"
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Input
                            name="name"
                            placeholder="Digite Seu Nome"
                            onChange={(e) => handleChange(e)}
                            value={form.name || ""}
                        />
                    </Form.Item>
                    <Form.Item
                        name="username"
                        label="Username"
                        rules={[
                            {
                                required: true,
                            },
                        ]}>
                        <Input
                            name="username"
                            placeholder="Digite seu nome de usuario"
                            onChange={(e) => handleChange(e)}
                            value={form.username || ""}
                        />
                    </Form.Item>
                    <Form.Item
                        name="email"
                        label="Email"
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Input
                            name="email"
                            placeholder="Entre com seu e-mail"
                            onChange={(e) => handleChange(e)}
                            value={form.email || ""}
                        />
                    </Form.Item>
                    <Form.Item
                        name="place"
                        label="Place"
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Input
                            name="place"
                            placeholder="Entre com sua Cidade"
                            onChange={(e) => handleChange(e)}
                            value={form.place || ""}
                        />
                    </Form.Item>

                    <Form.Item
                        name="gender"
                        label="Gender"
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Select
                            placeholder="Select a option and change input text above"
                            allowClear
                            name="gender"
                            onSelect={(e) => handleChange(e)}
                        >
                            <Select.Option value="Male">Male</Select.Option>
                            <Select.Option value="Female">Female</Select.Option>
                            <Select.Option value="Other">Other</Select.Option>
                        </Select>
                    </Form.Item>

                    <Form.Item
                        name="password"
                        label="Password"
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Input.Password name="password" placeholder="Entre com sua senha" onChange={(e) => handleChange(e)} value={form.password || ""} />
                    </Form.Item>



                    <Form.Item>
                        <Button type="primary" onClick={(e) => handleSubmit(e)} >
                            Enviar
                        </Button>
                        <Button type="primary" onClick={() => history.push('/login')}  >
                            Ja tem conta? Logar
                        </Button>

                    </Form.Item>

                </FormEstilo>
            </FormCadastro>
        </LayoutEstilo>
    )

}

const LayoutEstilo = styled(Layout)`

    height: 100vh;
    background-image: radial-gradient(circle, #73B1D6, #061b35);
    display: flex;
    justify-content: center;
    align-items: center;
`

const FormEstilo = styled(Form)`
    border-top: 1px solid #40A9FF;
    padding-top: 20px;
`

const FormCadastro = styled.div`

    width: 55%;
    background-color: white;
    padding: 30px;
    display: flex;
    justify-content: center;
    align-items: space-around;
    flex-direction: column;
    h2{
        text-align: center;
    }
    button{
        margin-right: 20px;
    }
   

`


export default SignUpForm