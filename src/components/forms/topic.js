import { Form, Input, Button } from "antd";
import { useState } from "react";
import styled from 'styled-components'
import { createTopic } from '../../service/topic'


const TopicForm = ({ trocarComponente, changeTopic }) => {


    const [form, setForm] = useState({})

    const handleChange = (event) => {
        const { value, name } = event.target;
        setForm({
            ...form,
            [name]: value,
        });

    }


    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            const { data } = await createTopic(form)
            setForm({})
            changeTopic(data)
            trocarComponente("showTopic")
        } catch (error) {
            console.log(error.message)
        }
    }

    return (
        <PageForm>
            <H2Estilo>Novo Topico | ReDDev</H2Estilo>
            <Form>
                <Form.Item name="title">
                    <Input
                        name="title"
                        label="Titulo"
                        placeholder="Digite o titulo do novo topico"
                        onChange={(e) => handleChange(e)}
                        value={form.title || ""}
                    />
                </Form.Item>
                <Form.Item name="description">
                    <Input.TextArea
                        name="description"
                        label="Descrição"
                        placeholder="Digite a descrição do novo topico"
                        onChange={(e) => handleChange(e)}
                        value={form.description || ""}
                    />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" onClick={(e) => handleSubmit(e)} style={{ "marginRight": "15px" }}>
                        Enviar
                    </Button>
                    <Button type="primary" onClick={() => trocarComponente("initialPage")}>
                        Voltar
                    </Button>

                </Form.Item>
            </Form>
        </PageForm>
    );

}

const PageForm = styled.div`
    padding: 50px 25%;
    max-height: 100vh;

`

const H2Estilo = styled.h2`
    text-align: center;
`



export default TopicForm