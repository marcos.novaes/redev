import SinglePost from "../singles/post"
import { Button } from 'antd'
import styled from "styled-components"
const Feed = (props) => {
    return (
        <>
            <ButtonStyled type="primary" >
                + Novo post
            </ButtonStyled>
            <SinglePost></SinglePost>
            <SinglePost></SinglePost>
            <SinglePost></SinglePost>
            <SinglePost></SinglePost>
            <SinglePost></SinglePost>
            <SinglePost></SinglePost>
            <SinglePost></SinglePost>
            <SinglePost></SinglePost>
        </>

    )
}

const ButtonStyled = styled(Button)`
    margin: 5px;
`

export default Feed