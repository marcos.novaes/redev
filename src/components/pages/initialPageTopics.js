import styled from "styled-components"
import Search from "antd/lib/input/Search"
import Carousel from 'react-elastic-carousel'
import SingleTopic from "../singles/topic"
import { Button } from "antd";
import { useEffect, useState } from "react";
import { getTopics } from '../../service/topic'


const InitialPageTopics = ({ trocarComponente, changeTopic }) => {
    const [topics, setTopics] = useState([])

    const handleClickOnTopic = (topic) => {
        changeTopic(topic)
        trocarComponente("showTopic")
    }



    useEffect(() => {
        const getTopic = async () => {
            try {
                const { data } = await getTopics()
                console.log(data)
                setTopics(data)
            } catch (error) {
                console.log(error.message)
            }
        }
        getTopic()
        console.log(topics);
    }, [])



    return (
        <DivPageTopics>
            <DivSearchJoin>
                <SearchEstilo
                    placeholder="Procure por um topico"
                    allowClear
                    enterButton="Search"
                    size="medium"
                />
                <Button type="primary" onClick={() => trocarComponente("form")}  >
                    + Criar
                </Button>
            </DivSearchJoin>
            <DivTopicos >
                <h2>Topicos Populares</h2>
                <CarouselEstilo itemsToShow={1}>
                    {topics.map((t, i) => <SingleTopic key={i} topic={t} handleClick={handleClickOnTopic}></SingleTopic>)}
                </CarouselEstilo>

            </DivTopicos>

            <DivTopicos >
                <h2>Você pode gostar</h2>
                <CarouselEstilo itemsToShow={1}>
                    {topics.map((t, i) => <SingleTopic key={i} topic={t} handleClick={handleClickOnTopic} ></SingleTopic>)}
                </CarouselEstilo>

            </DivTopicos>


        </DivPageTopics>



    )
}

const DivPageTopics = styled.div`

    width: 100%;
    height: auto;
    overflow: no-scroll;
    background: #fff;
    padding: 30px;
`

const DivSearchJoin = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-between;
`

const CarouselEstilo = styled(Carousel)`
    width: 100%;
    border-top: 1px solid #1890FF;

`

const DivTopicos = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    margin: 25px 0px;

`


const SearchEstilo = styled(Search)`
    width: 75%;
`

export default InitialPageTopics