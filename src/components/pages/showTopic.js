import styled from 'styled-components'
import SinglePost from "../singles/post"
import { Button } from 'antd'

const ShowTopic = ({ topic }) => {





    return (
        <ContainerPage>
            <TopicInfo>
                <h2>{topic.title} </h2>
                <small>{topic.participants_count} seguidores</small>
                <p>{topic.description}</p>
            </TopicInfo>
            <TopicFeed>
                <ButtonStyled type="primary" >
                    + Novo post
                </ButtonStyled>
                <SinglePost></SinglePost>
                <SinglePost></SinglePost>
                <SinglePost></SinglePost>
                <SinglePost></SinglePost>
                <SinglePost></SinglePost>
                <SinglePost></SinglePost>

            </TopicFeed>
        </ContainerPage>


    )
}

const ContainerPage = styled.div`
    width: 100%;
    overflow: no-scroll;
    padding: 5px 5px 5px 5px;
    height: 100vh;
   
`
const TopicInfo = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 100%;
    padding: 5px;
    border-bottom: 1px solid #001529;
    color: #fff;
    background-color: #1890FF;
    h2{
        color: #fff;
    }

`
const ButtonStyled = styled(Button)`
    margin: 5px;
`

const TopicFeed = styled.div`


`

export default ShowTopic