import { useState } from "react";
import TopicForm from '../forms/topic'
import InitialPageTopics from '../pages/initialPageTopics'
import ShowTopic from "./showTopic";

const Topics = (props) => {

    const [atualComponent, setAtualComponent] = useState("initialPage")
    const [topicAtual, setTopicAtual] = useState({})


    const changeComponent = (component) => {
        setAtualComponent(component)
    }

    const componentToShow = {
        "initialPage": <InitialPageTopics trocarComponente={changeComponent} changeTopic={setTopicAtual} ></InitialPageTopics>,
        "form": <TopicForm trocarComponente={changeComponent} changeTopic={setTopicAtual} ></TopicForm>,
        "showTopic": <ShowTopic topic={topicAtual}></ShowTopic>
    }




    return (
        componentToShow[atualComponent]



    )
}



export default Topics