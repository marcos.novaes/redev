import { Switch, Route, Router } from 'react-router-dom'
import history from './config/history'

import SignIn from './views/Sign/SignIn'
import BaseLayout from './components/layout/baseLayout'
import Perfil from './components/pages/perfil'
import SignUpForm from './components/forms/signup'
import Feed from './components/pages/feed'
import Topics from './components/pages/topic'


const Routes = () => (
    <Router history={history}>
        <Switch>
            <Route exact path="/login" component={SignIn} />
            <Route exact path="/cadastro" component={SignUpForm} />
            <BaseLayout>
                <Route exact path="/" component={Feed} />
                <Route exact path="/perfil" component={Perfil} />
                <Route exact path="/topicos" component={Topics} />
            </BaseLayout>

        </Switch>
    </Router>
)


export default Routes