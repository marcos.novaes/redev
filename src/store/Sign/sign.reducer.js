import { getToken, getUser } from "../../config/auth";
import { SIGN, SIGN_LOADING, SIGN_ERROR } from "./sign.action";

const INITIAL_STATE = {
    loading: false,
    token: getToken() || "",
    user: getUser() || {},
    hasError: false
};

const reducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SIGN_LOADING:
            state.loading = action.loading;
            return state;
        case SIGN:
            state.token = action.data.token;
            state.user = action.data.user;
            state.hasError = false;
            state.loading = false;
            return state;
        case SIGN_ERROR:
            state.loading = false;
            state.hasError = true;
            return state;
        default:
            return state;
    }
};

export default reducer;
