import { loginService } from "../../service/user";
import history from "../../config/history";
import { saveToken } from "../../config/auth";

export const SIGN = "SIGN";
export const SIGN_LOADING = "SIGN_LOADING";
export const SIGN_ERROR = "SIGN_ERROR";

export const signIn = (props) => {
  return async (dispatch) => {
    dispatch({ type: SIGN_LOADING, loading: true });
    try {
      const { data } = await loginService(props);
      dispatch({ type: SIGN, data: data });
      saveToken(data);
      history.push("/");
    } catch (error) {
      dispatch({ type: SIGN_ERROR });
    }
  };
};
