import { Layout, Col, Form, Input, Button } from "antd";
import styled from 'styled-components'
import signInBg from "../../assets/imgs/signInBg.jpeg";
import history from '../../config/history'
import { useState } from "react";
import { useDispatch } from "react-redux";
import { signIn } from "../../store/Sign/sign.action";

const { Content } = Layout;



const SignIn = () => {
    const dispatch = useDispatch();

    const [form, setForm] = useState({})

    const handleChange = (event) => {
        const { value, name } = event.target;
        setForm({
            ...form,
            [name]: value,
        });

    }


    const handleSubmit = (e) => {
        e.preventDefault()
        dispatch(signIn(form));
    }

    return (
        <Layout className="layout">
            <MainContent>
                <BlockBg span={16}>
                    <h2>ReDDev</h2>
                    <span>Import Experiencias as Codigo from Vida </span>
                    <BgImg />
                </BlockBg>
                <BlockForm span={8}>
                    <FormLogin>
                        <Form>
                            <Form.Item name="email">
                                <Input
                                    name="email"
                                    placeholder="Entre com seu e-mail"
                                    onChange={(e) => handleChange(e)}
                                    value={form.email || ""}
                                />
                            </Form.Item>

                            <Form.Item
                                name="password"
                                placeholder="Entre com sua senha"
                            >
                                <Input.Password
                                    placeholder="Entre com sua senha"
                                    name="password"
                                    onChange={(e) => handleChange(e)}
                                    value={form.password || ""}
                                />
                            </Form.Item>

                            <Form.Item>
                                <Button type="primary" onClick={(e) => handleSubmit(e)}>
                                    Enviar
                                </Button>

                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" onClick={() => history.push('/cadastro')} >
                                    Não tem conta? Cadastra-se
                                </Button>

                            </Form.Item>
                        </Form>
                    </FormLogin>

                </BlockForm>
            </MainContent>
        </Layout >
    );

}

const MainContent = styled(Content)`
    height: 100vh;
    width: 100%;
    display: flex;
`

const BlockBg = styled(Col)`
    height: 100vh;
    display: flex;
    position: relative;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    font-size: 5rem;
    font-weight: 700;
    font-family:  "Montserrat", sans-serif;

    color: #061b35;
    text-shadow: 4px 4px 2px #43949e;
    line-height: 1;
    span {
        font-family: "Montserrat", sans-serif;
        font-size: 20pt;
        font-weight: 400;
        text-shadow: none;
    }


`
const BlockForm = styled(Col)`
    background-color: red;
    height: 100vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
    background-image: radial-gradient(circle, #73B1D6, #061b35);
`

const FormLogin = styled.div`
  padding: 20px;
  width: 100%;
  align-self: center;
`;

const BgImg = styled.div`
    position: absolute;
    display: block;  
    background: url(${signInBg});
    background-size: contain;
    background-position: center;
    height: 100vh;
    width: 100%;
    opacity: 0.2;

`



export default SignIn