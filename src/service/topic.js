import clientHttp from "../config/clientHttp";


const createTopic = (data) => clientHttp.post("/topics", data);

const getTopics = () => clientHttp.get("/topics");



export {
    createTopic,
    getTopics
}