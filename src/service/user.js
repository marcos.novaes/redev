import clientHttp from "../config/clientHttp";


const loginService = (data) => clientHttp.post("/login", data);

const signUpService = (data) => clientHttp.post("/users", data);


export {
    loginService,
    signUpService
}